using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using BrandsShop.Controllers;
using BrandsShop.Data;
using BrandsShop.Data.Models;
using BrandsShop.Data.Services;
using BrandsShop.Data.ViewModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BrandsShopAPI.Test
{
    public class CategoryControllerTest
    {     
       private static DbContextOptions<AppDbContext> dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "BookDbControllerTest")
                .Options;

        AppDbContext context;
        CategoryServices categoryServices;
        CategoryController categoryController;
    
            [OneTimeSetUp]
            public void Setup()
            { 
                context = new AppDbContext(dbContextOptions);
                context.Database.EnsureCreated();

                SeedDatabase();

                categoryServices = new CategoryServices(context);
                categoryController = new CategoryController(categoryServices, new NullLogger<CategoryController>());
            }

            [Test, Order(1)]
            public void HTTPGET_GetAllCategories_WithSortBySearchPageNr_ReturnOk_Test()
            {
                IActionResult actionResult = categoryController.GetAllCategory();
                Assert.That(actionResult, Is.TypeOf<OkObjectResult>());
                var actionResultData = (actionResult as OkObjectResult).Value as List<Category>;
              
                Assert.That(actionResultData.Count, Is.EqualTo(6));

               
            }

        [Test, Order(2)]
        public void HTTPGET_GetCategoryById_ReturnsOk_Test()
        {
            int categoryId = 1;

            IActionResult actionResult = categoryController.GetAllCategoryByid(categoryId);

            Assert.That(actionResult, Is.TypeOf<OkObjectResult>());

            var categoryDate = (actionResult as OkObjectResult).Value as Category;
           
        }
        private void SeedDatabase()
            {
                var categories = new List<Category>
            {
                    new Category() {
                        Id = 1,
                        Name = "Category 1"
                    },
                    new Category() {
                        Id = 2,
                        Name = "Category 2"
                    },
                    new Category() {
                        Id = 3,
                        Name = "Category 3"
                    },
                    new Category() {
                        Id = 4,
                        Name = "Category 4"
                    },
                    new Category() {
                        Id = 5,
                        Name = "Category 5"
                    },
                    new Category() {
                        Id = 6,
                        Name = "Category 6"
                    },
            };
                context.Categories.AddRange(categories);


                context.SaveChanges();
            }


        }
    }
