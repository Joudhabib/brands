﻿using BrandsShop.Data;
using BrandsShop.Data.Models;
using BrandsShop.Data.Services;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandsShopAPI.Test
{
    class CategoryServicesTest
    {
        public static DbContextOptions<AppDbContext> dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "BrandsDBTest")
            .Options;
        AppDbContext context;
        CategoryServices categoryService;

        [OneTimeSetUp]
        public void Setup()
        {
            context = new AppDbContext(dbContextOptions);
            context.Database.EnsureCreated();

            SeedDatabase();

            categoryService = new CategoryServices(context);
        }
        [OneTimeTearDown]
        public void CleanUp()
        {
            context.Database.EnsureDeleted();
        }

        private void SeedDatabase()
        {
            var shopes = new List<Shopes>
            {
                    new Shopes() {
                        Id = 1,
                        NameShop = "shop 1"
                    },
                    new Shopes() {
                        Id = 2,
                        NameShop = "Brand 2"
                    },
                    new Shopes() {
                        Id = 3,
                        NameShop = "Brand 3"
                    },
                    new Shopes() {
                        Id = 4,
                        NameShop = "Brand 4"
                    },
                    new Shopes() {
                        Id = 5,
                        NameShop = "Brand 5"
                    },
                    new Shopes() {
                       Id  = 6,
                        NameShop = "Brand 6"
                    },
            };
            context.Shopes.AddRange(shopes);

            var categories = new List<Category>()
            {
                new Category()
                {
                    Id = 1,
                   Name = "Category 1"
                },
                new Category()
                {
                    Id = 2,
                    Name = "Category 2"
                },
                       
                    new Category()
                    {
                        Id = 3,
                        Name = "Category 3"
                    },
                    new Category()
                    {
                        Id = 4,
                        Name = "Category 4"
                    },
                    new Category()
                    {
                        Id = 5,
                        Name = "Category 5"
                    },
                    new Category()
                    {
                        Id = 6,
                        Name = "Category 6"
                    },
            }; 
            context.Categories.AddRange(categories);


            var brands = new List<Brand>()
            {
                new Brand()
                {
                    BrandId = 1,
                    Name = "Brand 1 Title",
                    Color = "red",
                    IsBuied = false,
                    Size = "XL",
                    ImageUrl = "https://...",
                    DateBuied = DateTime.Now.AddDays(-10),
                    CategoryId = 1
                },
                new Brand()
                {
                  BrandId = 2,
                    Name = "Brand 2 Title",
                    Color = "green",
                    IsBuied = true,
                    Size = "S",
                    ImageUrl = "https://...",
                    DateBuied = DateTime.Now.AddDays(-5),
                    CategoryId = 2
                }
            };
            context.Brands.AddRange(brands);

            var brands_shopes = new List<Shope_Brand>()
            {
                new Shope_Brand()
                {
                    Id = 1,
                    Brand_Id = 1,
                    ShopId = 1
                },
                new Shope_Brand()
                {
                    Id = 2,
                    Brand_Id = 1,
                    ShopId = 2
                },
                new Shope_Brand()
                {
                    Id = 3,
                    Brand_Id = 2,
                    ShopId = 2
                },
            };
            context.Shopes_Brands.AddRange(brands_shopes);


            context.SaveChanges();
        }

    }
}
  
