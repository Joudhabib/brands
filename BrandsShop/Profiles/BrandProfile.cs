﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Profiles
{
    public class BrandProfile : Profile
    {
        public BrandProfile() {
            CreateMap< Data.Models.Brand,Data.ViewModels.AllBrandWhihNamesViewModel>();
            CreateMap<Data.Models.Brand, Data.ViewModels.BrandViewModelForUpdate>().ReverseMap();

        } }
}
