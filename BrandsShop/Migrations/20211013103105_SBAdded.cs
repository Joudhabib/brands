﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BrandsShop.Migrations
{
    public partial class SBAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Shopes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameShop = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shopes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Shope_Brands",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Brand_Id = table.Column<int>(type: "int", nullable: false),
                    ShopId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shope_Brands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Shope_Brands_Brands_Brand_Id",
                        column: x => x.Brand_Id,
                        principalTable: "Brands",
                        principalColumn: "BrandId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Shope_Brands_Shopes_ShopId",
                        column: x => x.ShopId,
                        principalTable: "Shopes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shope_Brands_Brand_Id",
                table: "Shope_Brands",
                column: "Brand_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Shope_Brands_ShopId",
                table: "Shope_Brands",
                column: "ShopId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Shope_Brands");

            migrationBuilder.DropTable(
                name: "Shopes");
        }
    }
}
