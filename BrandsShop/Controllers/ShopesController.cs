﻿using AutoMapper;
using BrandsShop.Data.Services;
using BrandsShop.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopesController : ControllerBase
    {
        public BrandService _brandService;
        public ShopesService  _shopesService;
     
        public ShopesController (ShopesService shopesService)
        {
            _shopesService = shopesService;
    
      
        }
        [HttpPost("AddShop")]
        public IActionResult AddShop([FromBody] ShopViewModel shop)
        {

       var a= _shopesService.AddShop(shop);

            return Ok(a);
        }

        [HttpGet("GetAllShopes")]
        public IActionResult GetAllShopes ()
        {
        var a=  _shopesService.GetAllShops();
            return Ok(a);
        }
        [HttpGet("GetShopeById/{shopId}")]
        public IActionResult GetShopById(int shopId)
        {
            var r = _shopesService.GetShopById(shopId);
            return Ok(r);
        }

        [HttpPut("UpdateSd /{shopId}")]
        public IActionResult UpdateShopById(int shopId, ShopViewModel shop)
        {
            var r = _shopesService.UpdateShop(shopId, shop);
            return Ok(r);
        }
        [HttpDelete("deletesd/{shopId}")]
        public IActionResult DeleteShop(int shopId)
        {
            _shopesService.DeleteShopById(shopId);
            return Ok();

        }
        [HttpGet("GetBrandsForThisShopes/{shopId}")]
        public IActionResult GetBrandsForThisShop(int shopId)
        {

       var r=     _shopesService.GetShopWithBrands(shopId);

            return Ok(r);
        }
    }
}



