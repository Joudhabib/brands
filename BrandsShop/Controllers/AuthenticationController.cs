﻿using BrandsShop.Data.Models;
using BrandsShop.Data.Services;
using BrandsShop.Data.ViewModels.Authentication;
using BrandsShop.Data.ViewModels.Authentications;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly AuthService _authService;
        public AuthenticationController(AuthService authService)
        {
            _authService = authService;
        }
        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel registerViewModel)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var Result = await _authService.Register(registerViewModel);
            if (!Result.IsAuthenticated)
                return BadRequest(Result.Message);
            return Ok(Result);
        }
        [HttpPost("Token")]
        public async Task<IActionResult> GetTokenAsync([FromBody] TokentModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var Result = await _authService.GetTokenAsync(model);
            if (!Result.IsAuthenticated)
                return BadRequest(Result.Message);
            return Ok(Result);
        }
        [HttpPost("AddRole")]
        public async Task<IActionResult> AddRoleAsync(AddRoleModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var Result = await _authService.AddRoleAsync(model);
            if (!string.IsNullOrEmpty(Result))
                return BadRequest(Result);
            return Ok(Result);
        }
    }
}
