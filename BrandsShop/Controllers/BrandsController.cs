﻿using AutoMapper;
using BrandsShop.ResourseParameter;
using BrandsShop.Data.Models;
using BrandsShop.Data.Services;
using BrandsShop.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BrandsShop.Pagination;
using System.Text.Json;
using BrandsShop.Data;

namespace BrandsShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandsController : ControllerBase
    {

        public BrandService _brandService;
        public CategoryServices _categoryServices;
        public ShopesService _shopesService;
        private readonly IMapper _mapper;
        private AppDbContext _appDbContext;
        public BrandsController (AppDbContext appDbContext,BrandService brandService,IMapper mapper,CategoryServices categoryServices, ShopesService shopesService)
        {
            _appDbContext = appDbContext;
            _brandService = brandService;
            _mapper = mapper;
            _categoryServices = categoryServices;
            _shopesService = shopesService;

        }

        [HttpPost("AddBrand")]
        public IActionResult AddBrand([FromBody] BrandViewModel brand)
        {



                _brandService.AddBrand(brand);
            

            return Ok();
        }

        [HttpGet("GetAllBrands")]
        public IActionResult GetAll()
        {
       var brd=     _brandService.GetAllBrands();
            return Ok(brd);
        }
        [HttpGet("getAllBrandsOfCategoryName")]
        public IActionResult GetAllOfCategory (string categoryViewModel)
        {
            var brd = _brandService.GetAllBrandsOfCategory(categoryViewModel)
                ;
            return Ok(brd);
        }
        [HttpGet(Name = "GetBrands")]
       [HttpHead]
        public IActionResult GetBrands ([FromQuery] BrandResourceParameters brandResourceParmeters)

        {
         
            var brandsFromRepo = _brandService.GetBrands(brandResourceParmeters);
            var previousPageLink = brandsFromRepo.HasPrevious ?
         CreateBrandResourceUri(brandResourceParmeters, ResourceUriType.previousPage) : null;
            var nextPageLink = brandsFromRepo.HasNext ?
  CreateBrandResourceUri(brandResourceParmeters, ResourceUriType.NextPage) : null;
            var paginationMetaData = new
            {
                totalCount = brandsFromRepo.TotalCount,
                pageSize = brandsFromRepo.PageSize,
                currentPage = brandsFromRepo.CurrentPage,
                totalPage = brandsFromRepo.TotalPage,
                previousPageLink,
                nextPageLink
            };
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetaData));
                  
            return Ok(_mapper.Map<IEnumerable<AllBrandWhihNamesViewModel>>(brandsFromRepo));
        }
        [HttpGet("GetBrandById/{brandId}", Name = "GetBrand")]
        public IActionResult GetBrandById(int brandId)
        {
            var brd = _brandService.GetBrandById(brandId);
            return Ok(brd);
        }
        [HttpGet("GetAllShopesAndCategoryOfBrand")]
        public IActionResult GetAllShopesAndCategoryOfBrand (string name)
        {
            var brd = _brandService.GetAllShopesAndCategoryOfBrand(name);
            return Ok(brd);
        }
        [HttpPut("UpdateBrand/{brandId}")]
        public IActionResult UpdateBrand (int brandId, BrandViewModelForUpdate brand)
        {
            
            
        
         var a=   _brandService.UpdateBrands(brandId, brand);
            return Ok(a);







        }
        [HttpDelete("DeleteBrand/{brandId}")]
        public IActionResult DeleteBrand(int brandId)
        {
           _brandService.DeleteBrandById(brandId);
            return Ok();
          
        }
        private string CreateBrandResourceUri(BrandResourceParameters authorsResourceParameters, ResourceUriType type)
        {
            switch(type)
            {
                case ResourceUriType.previousPage:
                    return Url.Link("GetBrands",
                        new
                        {
                        
                            pageNumber = authorsResourceParameters.PageNumber - 1,
                            pageSize = authorsResourceParameters.PageSize,
                            mainCategory = authorsResourceParameters.WhatSize,
                            searchQuery = authorsResourceParameters.SearchQuery
                        });
                case ResourceUriType.NextPage:
                    return Url.Link("GetBrands",
                        new
                        {
                     
                            pageNumber = authorsResourceParameters.PageNumber + 1,
                            pageSize = authorsResourceParameters.PageSize,
                            mainCategory = authorsResourceParameters.WhatSize,
                            searchQuery = authorsResourceParameters.SearchQuery
                        });
                default:

                    return Url.Link("GetBrands",
                        new
                        {
                   
                            pageNumber = authorsResourceParameters.PageNumber,
                            pageSize = authorsResourceParameters.PageSize,
                            mainCategory = authorsResourceParameters.WhatSize,
                            searchQuery = authorsResourceParameters.SearchQuery
                        });
            }

        }
    }
}
