﻿using AutoMapper;
using BrandsShop.Data.Services;
using BrandsShop.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        public CategoryServices  _categoryServices;
      

        private readonly ILogger<CategoryController> _logger;



        public CategoryController(CategoryServices categoryServices,  ILogger<CategoryController> logger)
        {
            _categoryServices = categoryServices;
            _logger = logger;
        }

        [HttpPost("AddCategory")]
        public IActionResult AddCategory([FromBody] CategoryViewModel category)
        {
        var a=_categoryServices.AddCategory(category);

            return Ok(a);
        }

        [HttpGet("GetAllCategories")]
        public IActionResult GetAllCategory()
        {

            try
            {
       
                var _result = _categoryServices.GetAllCategories();
                return Ok(_result);
            }
            catch (Exception)
            {
                return BadRequest("Sorry, we could not load the publishers");
            }
        }
        [HttpGet("GetCategoryById/{id}")]
        public IActionResult GetAllCategoryByid(int id)
        {

            var g = _categoryServices.GetCategoryById(id);
            if (g != null)
            {
                return Ok(g);
            }
            else
            {
                return NotFound();
            }
        

       
        }
        [HttpGet("GetBrandsAndShopesOfCategory/{id}")]
        public IActionResult GetBrandsOgCategory (int id)
        {

            var g = _categoryServices.GetBrandAndShopesOfCategory(id);
            return Ok(g);
        }
        [HttpDelete("DeleteCategory/{category}")]
        public IActionResult DeleteCategory(int category)
        {
            _categoryServices.DeleteCategory(category);
            return Ok();
        }
        [HttpPut("UpdateCategory /{category}")]
        public IActionResult UbdateCategory (int category, CategoryViewModel categorywhithBrandsViewModel)

        {
           var g= _categoryServices.UbdateCategory(category, categorywhithBrandsViewModel);
            return Ok(g);
        }
    }
}
