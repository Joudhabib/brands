﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Models
{
    public class Shopes
    {
        public int Id { get; set; }
        public string NameShop  { get; set; }
        public List<Shope_Brand> Shope_Brands { get; set; }

    }
}
