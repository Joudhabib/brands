﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Models
{
    public class Shope_Brand
    {
        public int Id { get; set; }
        public int Brand_Id { get; set; }
        public Brand Brand { get; set; }
        public int ShopId { get; set; }
        public Shopes Shopes { get; set; }

    }
}
