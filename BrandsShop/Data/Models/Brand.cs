﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Models
{
    public class Brand
    {
        public  int BrandId { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public bool IsBuied { get; set; }
        public DateTime? DateBuied { get; set; }
        public int? Rate { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }
        public int? CategoryId { get; set; }
        public Category Category { get; set; }
        public List<Shope_Brand> Shope_Brands { get; set; }

    }
}
