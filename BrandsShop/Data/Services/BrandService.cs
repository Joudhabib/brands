﻿using AutoMapper;
using BrandsShop.Data.Models;
using BrandsShop.Data.ViewModels;
using BrandsShop.Pagination;
using BrandsShop.ResourseParameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Services
{
    public class BrandService
    {
        private AppDbContext _appDbContext;
        private readonly IMapper _mapper;
        public BrandService(AppDbContext appDbContext, IMapper mapper)
        {
            _appDbContext = appDbContext;
            _mapper = mapper;
        }
        public void AddBrand(BrandViewModel brand)
        {
            if (brand == null)
            {
                throw new ArgumentNullException(nameof(brand));
            }
            var brandI = new Brand()
            {
                Name = brand.Name,
                Size = brand.Size,
                Price = brand.Price,

                IsBuied = brand.IsBuied,
                Rate = brand.IsBuied ? brand.Rate.Value : null,
                ImageUrl = brand.ImageUrl,
                Color = brand.Color
            };

           _appDbContext.Brands.Add(brandI);
           _appDbContext.SaveChanges();
          //  foreach (var bs in brand.ShopesIds)
          //  {
            ///    var brand_shop = new Shope_Brand()
           //     {
           //         Brand_Id = brandI.BrandId,
            //        ShopId = bs

           //     };
           //     _appDbContext.Shopes_Brands.Add(brand_shop);
           //     _appDbContext.SaveChanges();
        //    }
        }
        public List<AllBrandWhihNamesViewModel> GetAllBrands()
        {
            return _appDbContext.Brands.Select(brand => new AllBrandWhihNamesViewModel()
            {
                BrandId = brand.BrandId,
                Name = brand.Name,
                Size = brand.Size,
                Price = brand.Price,
                IsBuied = brand.IsBuied,
                Rate = brand.IsBuied ? brand.Rate.Value : null,
                ImageUrl = brand.ImageUrl,
                CategoryName = brand.Category.Name,
                Color = brand.Color,
           //     ShopesNames = brand.Shope_Brands.Select(n => n.Shopes.NameShop).ToList()

            }).ToList();
        }
        public IEnumerable<BrandWhihNamesViewModel> GetAllBrandsOfCategory(string categoryViewModel)
        {
            return _appDbContext.Brands.Where(c => c.Category.Name == categoryViewModel).Select(brand => new BrandWhihNamesViewModel()
            {

                Name = brand.Name,
                Size = brand.Size,
                Price = brand.Price,
                IsBuied = brand.IsBuied,
                Rate = brand.IsBuied ? brand.Rate.Value : null,
                ImageUrl = brand.ImageUrl,
                CategoryName = brand.Category.Name,
                Color = brand.Color,
           //     ShopesNames = brand.Shope_Brands.Select(n => n.Shopes.NameShop).ToList()

            }).ToList();


        }
        public IEnumerable<ShopWhitBrandsViewModel> GetAllShopesAndCategoryOfBrand(string name)
        {
            return _appDbContext.Brands.Where(c => c.Name == name).Select(x => new ShopWhitBrandsViewModel()
            {
                Name = x.Category.Name,
               // BrandsCategory = x.Shope_Brands.Select(x => x.Shopes.NameShop).ToList()
            }).ToList();
        }


        public BrandWhihNamesViewModel GetBrandById(int brandId)
        {
            var getBrandWithNames = _appDbContext.Brands.Where(c => c.BrandId == brandId).Select(brand => new BrandWhihNamesViewModel()
            {

                Name = brand.Name,
                Size = brand.Size,
                Price = brand.Price,
                IsBuied = brand.IsBuied,
                Rate = brand.IsBuied ? brand.Rate.Value : null,
                ImageUrl = brand.ImageUrl,
                CategoryName = brand.Category.Name,
                Color = brand.Color,
             //   ShopesNames = brand.Shope_Brands.Select(n => n.Shopes.NameShop).ToList()

            }).FirstOrDefault();
            return getBrandWithNames;
        }

        public Brand UpdateBrands(int brandId, BrandViewModelForUpdate brand)
        {
            var brandII = _appDbContext.Brands.FirstOrDefault(c => c.BrandId == brandId);
            if (brandII != null)
            {

                //   brandII.Name = brand.Name;
                //   brandII.Price = brand.Price;
                //   brandII.Size = brand.Size;
                //   brandII.IsBuied = brand.IsBuied;
                //   brandII.Rate = brand.IsBuied ? brand.Rate.Value : null;
                //   brandII.ImageUrl = brand.ImageUrl;
                //   brandII.Color = brand.Color;
              

                _mapper.Map(brand, brandII);


                _appDbContext.SaveChanges();
            }
            return brandII;
        }




        public Response<Brand> GetBrands(BrandResourceParameters brandResourceParameters)
        {
            if (brandResourceParameters == null)
            {
                throw new ArgumentNullException(nameof(brandResourceParameters));
            }

            var collection = _appDbContext.Brands as IQueryable<Brand>;
            if (!string.IsNullOrWhiteSpace(brandResourceParameters.WhatSize))
            {
                var mainCategory = brandResourceParameters.WhatSize.Trim();
                collection = collection.Where(a => a.Size == mainCategory);
            }
            if (!string.IsNullOrWhiteSpace(brandResourceParameters.SearchQuery))
            {
                var SearchQuery = brandResourceParameters.SearchQuery.Trim();
                collection = collection.Where(a => a.Name.Contains(SearchQuery)
                  || a.Name.Contains(SearchQuery));
             
            }

         

            return Response<Brand>.Create(collection,
                brandResourceParameters.PageNumber,
                brandResourceParameters.PageSize);
        }







        public bool Save()
        {
            return (_appDbContext.SaveChanges() >= 0);

        }
        public void DeleteBrandById(int brandId)
        {
            var brandI = _appDbContext.Brands.FirstOrDefault(c => c.BrandId == brandId);
            _appDbContext.Brands.Remove(brandI);
            _appDbContext.SaveChanges();
        }
        }
}
