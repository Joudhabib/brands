﻿using BrandsShop.Data.Models;
using BrandsShop.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Services
{
    public class ShopesService
    {
        private AppDbContext _appDbContext;
        public ShopesService(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;

        }
        public Shopes AddShop(ShopViewModel Shop)
        {
          
            var shopI = new Shopes()
            {
                NameShop = Shop.Name
            };
            _appDbContext.Shopes.Add(shopI);
            _appDbContext.SaveChanges();
            return shopI; 
        }
        public List<Shopes> GetAllShops()
        {
            return _appDbContext.Shopes.ToList();
        }
        public Shopes GetShopById(int shopId)
        {
            return _appDbContext.Shopes.FirstOrDefault(c => c.Id == shopId);
        }
        public ShopWhitBrandsViewModel GetShopWithBrands(int shopId)
        {
            var s = _appDbContext.Shopes.Where(c => c.Id == shopId).Select(x => new ShopWhitBrandsViewModel()
            {
                Name = x.NameShop,
            //    BrandsCategory = x.Shope_Brands.Select(x => x.Brand.Name).ToList()
            }).FirstOrDefault();
            return s;
        }
    
        public Shopes UpdateShop(int shopId, ShopViewModel shop)
        {
            var shopI = _appDbContext.Shopes.FirstOrDefault(c => c.Id == shopId);
            if (shopI != null)
            {
                shopI.NameShop = shop.Name;
                _appDbContext.SaveChanges();
            }
            return shopI;

        }
        public void DeleteShopById(int shopId)
        {
            var shopI = _appDbContext.Shopes.FirstOrDefault(c => c.Id == shopId);
            _appDbContext.Shopes.Remove(shopI);
            _appDbContext.SaveChanges();
        }

    }
}
