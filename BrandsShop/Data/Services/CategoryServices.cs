﻿using BrandsShop.Data.Models;
using BrandsShop.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.Services
{
    public class CategoryServices
    {
        private AppDbContext _appDbContext;
        public CategoryServices(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;

        }


        public Category AddCategory(CategoryViewModel category)
        {
           
            var categoryI = new Category()
            {
                Name = category.CategoryName,

            };
            _appDbContext.Categories.Add(categoryI);
            _appDbContext.SaveChanges();
            return categoryI;
        } 
    public List<Category> GetAllCategories()
    {
        return _appDbContext.Categories.ToList();
    }
    public Category GetCategoryById(int id)
        
       => _appDbContext.Categories.FirstOrDefault(c => c.Id == id);
    
    public void AddCategoryWhithBrand(BrandCategoryViewModels category)
    {

        if (category == null)
        {
            throw new ArgumentNullException(nameof(category));
        }
        var brands = new Category()
        {
            Name = category.Name,

        };



        _appDbContext.Categories.Add(brands);
        _appDbContext.SaveChanges();
            

    } 


        public CategorywhithBrandsViewModel GetBrandAndShopesOfCategory(int id)
        {

            var f = _appDbContext.Categories.Where(c => c.Id == id).Select(c => new CategorywhithBrandsViewModel()
            {
                CategoryName = c.Name,
                Brands = c.Brands.Select(c => new BrandCategoryViewModels()
                {
                    Name = c.Name,
                 //   BrandCategory = c.Shope_Brands.Select(c => c.Shopes.NameShop).ToList()
                }).ToList()
            }).FirstOrDefault();
            return f;
        }
        public void DeleteCategory(int category)
        {
            var c = _appDbContext.Categories.FirstOrDefault(n => n.Id == category);
            
                _appDbContext.Categories.Remove(c);
                _appDbContext.SaveChanges();
            
        }
        public Category UbdateCategory(int category, CategoryViewModel categorywhithBrandsViewModel)
        {
            var c = _appDbContext.Categories.FirstOrDefault(n => n.Id == category);
            c.Name = categorywhithBrandsViewModel.CategoryName;
            _appDbContext.SaveChanges();
            return c;
        }
    }
}