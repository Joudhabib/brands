﻿using BrandsShop.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.ViewModels
{
    public class CategoryViewModel
    {
        public string CategoryName { get; set; }
   
    }

    public class CategorywhithBrandsViewModel
    {
        public string CategoryName { get; set; }
        public List<BrandCategoryViewModels> Brands { get; set; }
    }
    public class BrandCategoryViewModels
    {
        public string Name { get; set; }

        public List<string> BrandCategory { get; set; }
    }
  
}