﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.ViewModels.Authentications
{
    public class AddRoleModel 
    {
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}
