﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.ViewModels.Authentication
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "User Name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "User Name is required")]
        public string LastName { get; set; }


        [Required(ErrorMessage = "User Name is required")]
        public string Username { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }

    }
}