﻿using BrandsShop.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.ViewModels
{
    //update,create,delete
    public class BrandViewModel
    {
        public int BrandId { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public bool IsBuied { get; set; }
        public DateTime? DateBuied { get; set; }
        public int? Rate { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }

        public int CategoryId { get; set; }
        public List<int> ShopesIds { get; set; }
    }
    //get brandId
    public class BrandWhihNamesViewModel
    {

        public string Name { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public bool IsBuied { get; set; }
        public DateTime? DateBuied { get; set; }
        public int? Rate { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }

        public string CategoryName { get; set; }
        public List<string> ShopesNames { get; set; }
    }
    //get brandId
    public class AllBrandWhihNamesViewModel
    {

        public int BrandId { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public bool IsBuied { get; set; }
        public DateTime? DateBuied { get; set; }
        public int? Rate { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }

        public string CategoryName { get; set; }
        public List<string> ShopesNames { get; set; }
    }
    public class BrandViewModelForUpdate 
    {

        public string Name { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public bool IsBuied { get; set; }
        public DateTime? DateBuied { get; set; }
        public int? Rate { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }
      
        public List<int> ShopesIds { get; set; }
    }
}