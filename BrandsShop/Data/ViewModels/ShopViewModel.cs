﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data.ViewModels
{
    public class ShopViewModel
    {
        public string Name { get; set; }
    }

    public class ShopWhitBrandsViewModel
    {
        public string Name { get; set; }
        public List<string> BrandsCategory  { get; set; }
    }
  
}
