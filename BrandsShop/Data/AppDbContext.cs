﻿using BrandsShop.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrandsShop.Data
{
    public class AppDbContext :IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
           : base(options)
        {
        }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Shopes> Shopes { get; set; }
        public DbSet<Shope_Brand> Shopes_Brands  { get; set; }

        public DbSet<Category> Categories  { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shope_Brand>()
               .HasOne(b => b.Brand)
               .WithMany(ba => ba.Shope_Brands)
               .HasForeignKey(bi => bi.Brand_Id); 

            modelBuilder.Entity<Shope_Brand>()
              .HasOne(b => b.Shopes)
              .WithMany(ba => ba.Shope_Brands)
              .HasForeignKey(bi => bi.ShopId);

        
            // seed the database with dummy data
            modelBuilder.Entity<Brand>().HasData(
               
                 new Brand()
                 {
                     BrandId=1,
                 Name="Tichert1",
                 Size="S",
                 Price= 115.9M,
                 IsBuied=true,
                 Rate=4,
                 ImageUrl= "https://s4.forcloudcdn.com/item/images/dmc/7b98944e-1d75-4501-b113-520b67a5ddbf-800x800.jpg_495.jpg",
                 Color= "White"
                 
                 },
               new Brand()
               {BrandId=2,
                   Name = "Tichert2",
                   Size = "S",
                   Price = 117M,
                   IsBuied = true,
                   Rate = 3,
                   ImageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTH9iZR7xZHSfRbd4rpM-5v5_KVWf_kUJJ9cw&usqp=CAU",
                   Color = "blue"
               });

            base.OnModelCreating(modelBuilder);

        }
    } }