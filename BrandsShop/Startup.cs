using AutoMapper;

using BrandsShop.Data;
using BrandsShop.Data.Models;
using BrandsShop.Data.Services;
using BrandsShop.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BrandsShop
{
    public class Startup
    {
        public string ConnectionString { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConnectionString = Configuration.GetConnectionString("DefaultConnectionString");
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<JWT>(Configuration.GetSection("JWT"));
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<AppDbContext>();

            services.AddHealthChecks();

            services.AddControllers();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(ConnectionString));
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddTransient<BrandService>();
            services.AddTransient<CategoryServices>();
            services.AddTransient<ShopesService>();
            services.AddScoped<AuthService>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.SaveToken = false;
                o.TokenValidationParameters = new TokenValidationParameters
                {

                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidIssuer = Configuration["JWT:Issure"],
                    ValidAudience = Configuration["JWT:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Key"]))
                };
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v2", new OpenApiInfo { Title = "BrandsShop_updated_title", Version = "v2" });


            });
           services.AddHealthChecks().AddSqlServer(ConnectionString).AddUrlGroup(new Uri("https://localhost:44341/swagger/index.html"));
      
            services.AddHealthChecks();
            //   .AddCheck("SQl Server Check", () =>
            //   {
            //  using (var connection = new SqlConnection(ConnectionString))
            //  {

            //   try
            //   {
            //  connection.Open();
            //      return HealthCheckResult.Healthy();

            //   }
            ////  catch
            //  {
            //   return HealthCheckResult.Unhealthy();
            ///   }
            //   }
            //   })
            //  .AddCheck("Test Api Check",()=>
            //         {

            //     string website = "https://localhost:44341/swagger/index.html";
            //      using(HttpClient client=new HttpClient())
            //       {
            //      try
            //    {
            //                   var response = client.GetStringAsync(website).Result;
            //     if (response != null)
            //   {
            //      return HealthCheckResult.Healthy();
            //  }
            ///  else
            //    {
            //       return HealthCheckResult.Unhealthy();
            //   }
            //  }
            //     catch(Exception ex)
            //  {
            //       return HealthCheckResult.Unhealthy();
            //   }
            //  }


            //        });
        } 
            // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                    app.UseSwagger();
                    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v2/swagger.json", "BrandsShop_ui_updated v1"));
                }
            if (env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }
                app.UseRouting();
                app.UseAuthentication();
                app.UseAuthorization();
            app.UseHealthChecksUI();
                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapHealthChecks("/health" ,new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions(){
                        AllowCachingResponses=false,
                        ResponseWriter= HealthCheckResponserWrite
                });
                    endpoints.MapHealthChecks("/healthui", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions()
                    {
                        Predicate = (x) => { return true; },
                        ResponseWriter = HealthChecks.UI.Client.UIResponseWriter.WriteHealthCheckUIResponse
                    });
                    app.UseHealthChecksUI(options =>
                    {
                        options.UIPath = "/healthchecks-ui";
                        options.ApiPath = "/health-ui-api";
                    });
                });
        }

        private  Task HealthCheckResponserWrite(HttpContext context, HealthReport report)
        {
            context.Response.ContentType = "application/json";

            JObject obj = new JObject(
                new JProperty("Overall status", report.Status.ToString()),
                new JProperty("Time took", report.TotalDuration.TotalSeconds.ToString("0:0:00")),

                        new JProperty("Dependency Health Checks", new JObject(
                            report.Entries.Select(item =>
                            new JProperty(item.Key,
                            new JObject(

                        new JProperty("Status", item.Value.Status.ToString()),
                        new JProperty("Duration", item.Value.Duration.TotalSeconds.ToString("0:0:00"))
                         ))
                            ))
                        ));
            return context.Response.WriteAsync(obj.ToString(Newtonsoft.Json.Formatting.Indented));
        }
    } } 